﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalProgDiciembre
{
    //ESTA CLASE PODRIA LLAMARSE "RESPUESTA", PARA SER MAS GENERICA Y QUE PUEDA USARSE PARA OTRAS COSAS TAMBIEN
    public class DevolverError
    {
        public bool OcurrioError { get; set; }
        public string MensajeDeError { get; set; }

        public DevolverError()
        {

        }

        public DevolverError(bool ocurrioError)
        {
            OcurrioError = ocurrioError;
        }

        public void ActualizarMensaje(string mensaje)
        {
            OcurrioError = true;
            MensajeDeError = mensaje;
        }
    }
}
