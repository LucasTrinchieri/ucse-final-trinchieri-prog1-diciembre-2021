﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalProgDiciembre
{
    public class Evento
    {
        public Enumeradores.Evento TipoEvento { get; set; }
        public DateTime FechaEvento { get; set; }

        public Evento()
        {

        }

        public Evento(Enumeradores.Evento evento, DateTime fecha)
        {
            TipoEvento = evento;
            FechaEvento = fecha;
        }
    }
}
