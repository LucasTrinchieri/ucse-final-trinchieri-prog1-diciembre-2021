﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalProgDiciembre
{
    public class Enumeradores
    {
        public enum Estado
        {
            PENDIENTE_DE_ENVIO,
            ENVIADO,
            ULTIMO_TRAMO_DEL_RECORRIDO,
            ENTREGADO
        }

        public enum Evento
        {
            LLEGADA_AL_CENTRO_DE_DISTRIBUCION,
            EN_VIAJE,
            EN_MANOS_DEL_REPARTIDOR,
            ENTREGADO
        }
    }
}
