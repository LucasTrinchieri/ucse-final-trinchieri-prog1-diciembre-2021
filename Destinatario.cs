﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalProgDiciembre
{
    public class Destinatario : Persona
    {
        public int Localidad { get; set; } //codigo postal
        public string Direccion { get; set; }
    }
}
