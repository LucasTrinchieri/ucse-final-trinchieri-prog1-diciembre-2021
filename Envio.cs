﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalProgDiciembre
{
    public class Envio
    {
        public int Codigo { get; set; }
        public int DniDestinatario { get; set; }
        public int DniRepartidor { get; set; }
        public Enumeradores.Estado Estado { get; set; }
        public DateTime FechaEstimadaEntrega { get; set; }
        public DateTime FechaEntrega { get; set; }
        public List<Evento> HistorialEventos { get; set; }
        public string Descripcion { get; set; }

        public Envio()
        {

        }

        public Envio(int cod, int dniDestinatario, DateTime fechaEstimada, string descripcion)
        {
            Codigo = cod;
            DniDestinatario = dniDestinatario;
            FechaEstimadaEntrega = fechaEstimada;
            Descripcion = descripcion;

            Estado = Enumeradores.Estado.PENDIENTE_DE_ENVIO;

            HistorialEventos.Add(new Evento(Enumeradores.Evento.LLEGADA_AL_CENTRO_DE_DISTRIBUCION, DateTime.Now));
        }

        public DevolverError ActualizarEstado(Enumeradores.Evento evento)
        {
            //HistorialEventos.Last() PODRIA ASIGNARSE A UNA VARIABLE PARA EVITAR DUPLICARLO

            DevolverError devolverError = new DevolverError(false);

            switch (evento)
            {
                case Enumeradores.Evento.EN_VIAJE:
                    if (HistorialEventos.Last().TipoEvento == Enumeradores.Evento.LLEGADA_AL_CENTRO_DE_DISTRIBUCION)
                    {
                        Estado = Enumeradores.Estado.ENVIADO;
                        HistorialEventos.Add(new Evento(evento, DateTime.Now));
                    }
                    else
                    {
                        devolverError.ActualizarMensaje("El evento deberia ser LLEGADA_AL_CENTRO_DE_DISTRIBUCION");
                    }
                    break;

                case Enumeradores.Evento.EN_MANOS_DEL_REPARTIDOR:
                    if (HistorialEventos.Last().TipoEvento == Enumeradores.Evento.EN_VIAJE)
                    {
                        Estado = Enumeradores.Estado.ULTIMO_TRAMO_DEL_RECORRIDO;
                        HistorialEventos.Add(new Evento(evento, DateTime.Now));
                    }
                    else
                    {
                        devolverError .ActualizarMensaje("El evento deberia ser EN_VIAJE");
                    }
                    break;

                case Enumeradores.Evento.ENTREGADO:
                    if (HistorialEventos.Last().TipoEvento == Enumeradores.Evento.EN_MANOS_DEL_REPARTIDOR)
                    {
                        Estado = Enumeradores.Estado.ENTREGADO;
                        HistorialEventos.Add(new Evento(evento, DateTime.Now));
                        FechaEntrega = DateTime.Now;
                    }
                    else
                    {
                        devolverError.ActualizarMensaje("El evento deberia ser EN_MANOS_DEL_REPARTIDO");
                    }
                    break;
            }

            return devolverError;
        }

        public void AsignarRepartidor(int dni)
        {
            DniRepartidor = dni;
        }
    }
}
