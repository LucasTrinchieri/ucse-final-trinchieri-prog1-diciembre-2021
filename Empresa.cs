﻿namespace FinalProgDiciembre
{
    public class Empresa
    {
        List<Envio> envios = new List<Envio>();

        List<Repartidor> repartidores = new List<Repartidor>();
        List<Destinatario> destinatarios = new List<Destinatario>();

        public int CargarEnvio(int dniDestinatario, DateTime fechaEstimada, string descripcion)
        {
            envios.Add(new Envio(envios.Count + 1, dniDestinatario, fechaEstimada, descripcion));

            //DEBERIA RETORNAR EL ID ASIGNADO AL OBJETO ENVIO CREADO, NO LA LISTA, POR EJ:
            /*
                Envio nuevoEnvio = new Envio();
                return nuevoEnvio.Codigo;
            */
            return envios.Count + 1;
        }

        public DevolverError ActualizarEstado(int nroEnvio, Enumeradores.Evento evento)
        {
            //SI EL ENVIO NO EXISTE GENERA UNA EXCEPCION, SE PUEDE USAR FIRSTORDEFAULT PARA EVITARLO
            Envio envio = envios.First(x => x.Codigo == nroEnvio);

            return envio.ActualizarEstado(evento);
        }

        public void AsignarRepartidor()
        {
            List<Envio> enviosSinRepartidor = new List<Envio>();

            enviosSinRepartidor.AddRange(envios.Where(x => x.DniRepartidor == 0));

            //LAS LINEAS DE ARRIBA SE ESCRIBEN DE ESTA MANERA
            //List<Envio> enviosSinRepartidor = envios.Where(x => x.DniRepartidor == 0).ToList();

            foreach (Envio item in enviosSinRepartidor)
            {
                Destinatario destinatario = destinatarios.FirstOrDefault(x => x.Dni == item.DniDestinatario);
                //SI NO EXISTIA DESTINATARIO, ES NULL, Y GENERA UNA EXCEPCION.
                int dni = repartidores.First(x => x.ListadoCodigosPostales.Exists(x => x == destinatario.Localidad)).Dni;

                item.AsignarRepartidor(dni);
            }
        }
    }
}